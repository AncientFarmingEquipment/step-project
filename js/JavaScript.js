const ourWorksTabs = document.querySelector('.our-works-list');
const allWorksInOurWorks = document.querySelectorAll('.flip-card');
const ourWorksLoadMoreBtn = document.querySelector('.our-works-btn');
const ourWorksLoader = document.querySelector('.our-works-loader');
const ourServiceTabs = document.querySelector('.our-services-list');
const loading = document.querySelector('#loading');
const feedbackPrevBtn = document.querySelector('.previous-btn');
const feedbackNextBtn = document.querySelector('.next-btn');


ourServiceTabs.addEventListener('click', (e) => {
    let previousActiveTarget = document.querySelector('.our-services-list .active');
    if (e.target !== e.currentTarget && e.target.classList.contains('our-services-item')) {
        previousActiveTarget.classList.remove('active');
        document.querySelector('.our-services-item-content.show').classList.remove('show');
        e.target.classList.add('active');
        document.querySelector(`[data-content="${e.target.innerText.toLowerCase().trim()}"]`).classList.add('show');
    }
})

if (allWorksInOurWorks.length > 12) {
    ourWorksLoadMoreBtn.classList.remove('hide');
}

ourWorksLoadMoreBtn.addEventListener('click', () => {
    ourWorksLoader.classList.remove('hide');
    setTimeout(() => {
        ourWorksLoader.classList.add('hide');
        const countedHidenElements = document.querySelectorAll('.flip-card.flip-card-display-none');
        console.log(countedHidenElements.length)
        if(allWorksInOurWorks.length > countedHidenElements.length) {
            let count = 0;
            for (let i = 0; i < 12; i++) {
                countedHidenElements[i].classList.remove('flip-card-display-none');
            }
        }
        if(!(document.querySelector('.flip-card.flip-card-display-none'))) ourWorksLoadMoreBtn.classList.add('hide');
    }, 2000)
})

ourWorksTabs.addEventListener('click', (e) => {
    if (e.target.classList.contains('our-works-item-active')) return;
    allWorksInOurWorks.forEach(work => {
        work.classList.add('flip-card-display-none');
    })
    if (e.target !== e.currentTarget && e.target.classList.contains('our-works-item')) {
        document.querySelector('.our-works-list .our-works-item-active').classList.remove('our-works-item-active');
        e.target.classList.add('our-works-item-active');
        const currentCategory = e.target.innerText.toLowerCase().trim() === 'all' ? allWorksInOurWorks : document.querySelectorAll(`[data-work-category="${e.target.innerText.toLowerCase().trim()}"]`);
        if (currentCategory.length > 12) {
            ourWorksLoadMoreBtn.classList.remove('hide');
            for (let i = 0; i < 12; i++) {
                allWorksInOurWorks[i].classList.remove('flip-card-display-none');
            }
        } else {
            ourWorksLoadMoreBtn.classList.add('hide');
            currentCategory.forEach(work => {
                work.classList.remove('flip-card-display-none');
            })
        }
    }
})

let previousActiveCommentator = document.querySelector('.feedback-list-item-active');
let currentCommentator = +(previousActiveCommentator.dataset.commentator);

let currentCommentatorImg = document.querySelector('.up-img');
let currentSmallImg = +(currentCommentatorImg.dataset.smallImg);

function switchActiveElementsInFeedback() {
    document.querySelector(`[data-commentator="${currentCommentator}"]`).classList.add('feedback-list-item-active');
    previousActiveCommentator = document.querySelector(`[data-commentator="${currentCommentator}"]`);
    document.querySelector(`[data-small-img="${currentSmallImg}"]`).classList.add('up-img');
    currentCommentatorImg = document.querySelector(`[data-small-img="${currentSmallImg}"]`);
}

feedbackPrevBtn.addEventListener('click', () => {
    previousActiveCommentator.classList.remove('feedback-list-item-active');
    currentCommentatorImg.classList.remove('up-img');
    if (currentCommentator === 1) {
        currentCommentator = 4;
        currentSmallImg = 4;
        switchActiveElementsInFeedback();
    } else {
        currentCommentator--;
        currentSmallImg--;
        switchActiveElementsInFeedback();
    }
})

feedbackNextBtn.addEventListener('click', () => {
    previousActiveCommentator.classList.remove('feedback-list-item-active');
    currentCommentatorImg.classList.remove('up-img');
    if (currentCommentator === 4) {
        currentCommentator = 1;
        currentSmallImg = 1;
        switchActiveElementsInFeedback();
    } else {
        currentCommentator++;
        currentSmallImg++;
        switchActiveElementsInFeedback();
    }
})

const feedbackCommentsSliderUl = document.querySelector('.feedback-comments-slider');

feedbackCommentsSliderUl.addEventListener('click', (e) => {
    if (e.target !== e.currentTarget && e.target.classList.contains('feedback-commentator-photo-small')) {
        if (!e.target.classList.contains('up-img')) {
            previousActiveCommentator.classList.remove('feedback-list-item-active');
            currentCommentatorImg.classList.remove('up-img');
            currentCommentator = +(e.target.dataset.smallImg);
            currentSmallImg = currentCommentator;
            switchActiveElementsInFeedback();
        }
    }
})